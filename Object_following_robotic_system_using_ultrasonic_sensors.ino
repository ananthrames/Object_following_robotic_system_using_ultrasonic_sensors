//Human Following using 3 Ultrasonic Sensors :-)

// install Adafruit MotorShield V1 Library

#include <AFMotor.h>

AF_DCMotor lm(1); //left motor
AF_DCMotor rm(2); //right motor

// lm - left motor
// rm - right motor

// f - front/forward
// r - right
// l - left

// defines pins numbers
const int trigPinf = 30;  // forward facing Ultrasonic
const int echoPinf = 31;
long durationf;
int distancef;

const int trigPinl = 34;  // left facing Ultrasonic
const int echoPinl = 35;
long durationl;
int distancel;

const int trigPinr = 38;  // right facing Ultrasonic
const int echoPinr = 39;
long durationr;
int distancer;

 uint8_t i;

void setup() {
  // put your setup code here, to run once:
  pinMode(trigPinf, OUTPUT); // Sets the trigPin as an Output
  pinMode(echoPinf, INPUT); // Sets the echoPin as an Input

  pinMode(trigPinl, OUTPUT); // Sets the trigPin as an Output
  pinMode(echoPinl, INPUT); // Sets the echoPin as an Input

  pinMode(trigPinr, OUTPUT); // Sets the trigPin as an Output
  pinMode(echoPinr, INPUT); // Sets the echoPin as an Input
  
  Serial.begin(9600);           // set up Serial library at 9600 bps
  while (!Serial) ; // wait for Arduino Serial Monitor to open
  Serial.println("Serial Monitor Ready...");
  /*Serial1.begin(9600);
   while (!Serial1) ;
  Serial.println("Bluetooth Ready...");*/

   // turn on motor
   rm.setSpeed(200);
   rm.run(RELEASE);

   lm.setSpeed(200);
   lm.run(RELEASE);
}

void loop() {
  // put your main code here, to run repeatedly:
 if(ultrasonicf()>8 && ultrasonicf()<22)
 {
 runforward();
 }
 else if(ultrasonicf()<10 && ultrasonicf>2)
 {
  runbackward();
 } 
 else if(ultrasonicl()>7 && ultrasonicl()<17)
 {
  runleft();
 }
 else if(ultrasonicr()>7 && ultrasonicr()<17)
 {
  runright();
 }
}

void runright()
{
    lm.setSpeed(200);  
    lm.run(FORWARD);
    delay(10);
    lm.run(RELEASE);
  }

void runleft(){
    rm.setSpeed(200);  
    rm.run(FORWARD);
    delay(10);
    rm.run(RELEASE);
} 

void runforward()
{
    rm.setSpeed(150);
    lm.setSpeed(150); 
    rm.run(FORWARD);
    lm.run(FORWARD); 
    delay(10);
    rm.run(RELEASE);
    lm.run(RELEASE);
 }
 

void runbackward()
{
    rm.setSpeed(120);
    lm.setSpeed(120); 
    rm.run(BACKWARD);
    lm.run(BACKWARD); 
    delay(10);
    rm.run(RELEASE);
    lm.run(RELEASE);
}


int ultrasonicf()
{
  // Clears the trigPin
digitalWrite(trigPinf, LOW);
delayMicroseconds(2);
// Sets the trigPin on HIGH state for 10 micro seconds
digitalWrite(trigPinf, HIGH);
delayMicroseconds(10);
digitalWrite(trigPinf, LOW);
// Reads the echoPin, returns the sound wave travel time in microseconds
durationf = pulseIn(echoPinf, HIGH);
// Calculating the distance
distancef= durationf*0.034/2;
// Prints the distance on the Serial Monitor
Serial.print("Distance from front: ");
Serial.println(distancef);
return distancef;
}

int ultrasonicl()
{
  // Clears the trigPin
digitalWrite(trigPinl, LOW);
delayMicroseconds(2);
// Sets the trigPin on HIGH state for 10 micro seconds
digitalWrite(trigPinl, HIGH);
delayMicroseconds(10);
digitalWrite(trigPinl, LOW);
// Reads the echoPin, returns the sound wave travel time in microseconds
durationl = pulseIn(echoPinl, HIGH);
// Calculating the distance
distancel= durationl*0.034/2;
// Prints the distance on the Serial Monitor
Serial.print("Distance from left: ");
Serial.println(distancel);
return distancel;
}

int ultrasonicr()
{
  // Clears the trigPin
digitalWrite(trigPinr, LOW);
delayMicroseconds(2);
// Sets the trigPin on HIGH state for 10 micro seconds
digitalWrite(trigPinr, HIGH);
delayMicroseconds(10);
digitalWrite(trigPinr, LOW);
// Reads the echoPin, returns the sound wave travel time in microseconds
durationr = pulseIn(echoPinr, HIGH);
// Calculating the distance
distancer= durationr*0.034/2;
// Prints the distance on the Serial Monitor
Serial.print("Distance from right: ");
Serial.println(distancer);
return distancer;
}


